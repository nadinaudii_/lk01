package com.company;


public class Tiket {
    private String nama;
    private String asal;
    private String tujuan;

    //memberikan nilai pada tiket Komuter
    public Tiket(String nama) {
        this.nama = nama;
    }

    //memberikan nilai pada tiket KAJJ
    public Tiket(String nama, String asal, String tujuan) {
        this.nama = nama;
        this.asal = asal;
        this.tujuan = tujuan;
    }

    //mengubah nilai ke dalam bentuk string untuk menampilkan informasi tiket pada output
    public String toString(){
        if (asal == null || tujuan == null) {
            return "Nama : " +nama;
        }
        else {
            return "Nama : " +nama + "\n"+ "Asal : " + asal + "\n" + "Tujuan : " +tujuan
                    + "\n" + "------------------------------------" ;
        }
    }
}