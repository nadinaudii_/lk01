package com.company;
import java.util.*;

public class Kereta {
    String jenisKereta;
    int tiketTersedia;
    private ArrayList<Tiket> daftarPenumpang= new ArrayList<>();

    //default constructor
    //menginisialisasi nilai jenisKereta dan tiketTersedia ketika objek Kereta dibuat tanpa parameter.
    public Kereta() {
        this.jenisKereta = "Komuter";
        this.tiketTersedia = 1000;
    }
    //(overload constructor)
    //menginisialisasi nilai jenisKereta dan tiketTersedia dengan nilai yang ditentukan ketika objek Kereta dibuat dengan dua parameter.
    public Kereta(String jenisKereta, int tiketTersedia) {
        this.jenisKereta = jenisKereta;
        this.tiketTersedia = tiketTersedia;
    }
    //overload method(memiliki nama yang sama dengan method lain tetapi memiliki parameter yang berbeda)
    //membuat method untuk tiket Komuter
    public void tambahTiket(String nama) {
        if (daftarPenumpang.size() < tiketTersedia) {
            Tiket tiket = new Tiket(nama);
            this.daftarPenumpang.add(tiket);
            System.out.println("Tiket berhasil ditambahkan.");
            System.out.println("======================================================================");
        } else {
            System.out.println("Kereta telah habis dipesan, Silahkan cari jadwal keberangkatan lainnya");
        }
    }

    //overload method
    //membuat method untuk tiket KAJJ
    public void tambahTiket(String nama, String asal, String tujuan) {
        if (tiketTersedia>0) {
        Tiket tiket = new Tiket(nama, asal, tujuan);
        daftarPenumpang.add(tiket);
        System.out.println("======================================================================");
        System.out.print("Tiket berhasil dipesan.");
        tiketTersedia--;
        System.out.println("Sisa tiket tersedia :"+tiketTersedia);}
        else {
        System.out.println("======================================================================");
        System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        System.out.println("======================================================================");
    }
    }
    //membuat method untuk menampilkan tiket Komuter dan KAJJ
    public void tampilkanTiket() {
        System.out.println("Daftar penumpang kereta api " + jenisKereta + ":");
        System.out.println("------------------------------------");
        for (Tiket tiket : daftarPenumpang) {
            System.out.println(tiket.toString());
        }
        System.out.println();
    }
}

